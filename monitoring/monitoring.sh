#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

prometheus:setup_remote_influx() {
  local remote_storage_url="${1}"
  if [[ "remote_storage_url" != http* ]]; then
    remote_storage_url="https://${remote_storage_url}"
  fi

  printf "Setting up remote ${BLUE}influx storage${NC} for ${BLUE}prometheus${NC}\n"
  ${KUBECTL_CMD} apply -f "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/remote-influx.yaml" #&>dev/null

  printf "Creating remote ${BLUE}influx service${NC}\n"
  cat <<EOF | ${KUBECTL_CMD} apply -f -
apiVersion: monitoring.coreos.com/v1
kind: Prometheus
metadata:
  labels:
    prometheus: k8s
  name: k8s
  namespace: monitoring
spec:
  alerting:
    alertmanagers:
    - name: alertmanager-main
      namespace: monitoring
      port: web
  baseImage: quay.io/prometheus/prometheus
  nodeSelector:
    beta.kubernetes.io/os: linux
  remoteRead:
  - url: ${remote_storage_url}/api/v1/prom/read?db=prometheus
    read_recent: false
  remoteWrite:
  - url: ${remote_storage_url}/api/v1/prom/write?db=prometheus
  replicas: 2
  resources:
    requests:
      memory: 400Mi
  ruleSelector:
    matchLabels:
      prometheus: k8s
      role: alert-rules
  serviceAccountName: prometheus-k8s
  serviceMonitorSelector:
    matchExpressions:
    - key: k8s-app
      operator: Exists
  version: v2.2.1
EOF
}

prometheus:setup_operator() {
  printf "Installing ${BLUE}prometheus operator${NC}\n"
  ${KUBECTL_CMD} apply -f https://raw.githubusercontent.com/kubernetes/kops/master/addons/prometheus-operator/v0.19.0.yaml #&>/dev/null
}

prometheus:grafana_ingress() {
  local domain="${1}"
  printf "Creating grafana ingress ${BLUE}grafana.${domain}${NC}\n"
  cat <<EOF | ${KUBECTL_CMD} apply -f - &>/dev/null
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: grafana
  namespace: monitoring
  annotations:
    kubernetes.io/ingress.class: "nginx"
    nginx.ingress.kubernetes.io/force-ssl-redirect: "true"
spec:
  rules:
  - host: grafana-${domain}
    http:
      paths:
      - backend:
          serviceName: grafana
          servicePort: 3000
EOF
}

prometheus:create() {
  local domain="${1}"
  prometheus:setup_operator
  prometheus:grafana_ingress "${domain}"
}

