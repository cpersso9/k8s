#!/usr/bin/env bash

source "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/../commons/commons.sh"

external_dns:create() {
  printf "Creating ${BLUE}external-dns${NC}\n"

  local cluster_name=$1
  local domain=$2
  cat <<EOF | ${KUBECTL_CMD} apply -f - &>/dev/null
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: external-dns
spec:
  strategy:
    type: Recreate
  template:
    metadata:
      labels:
        app: external-dns
    spec:
      containers:
      - name: external-dns
        image: registry.opensource.zalan.do/teapot/external-dns:latest
        args:
        - --source=ingress
        - --domain-filter=$domain
        - --provider=aws
        - --policy=sync
        - --aws-zone-type=public
        - --registry=txt
        - --txt-owner-id=$cluster_name
EOF

}

#external_dns:create test.k8s k8s.sparetimecoders.com
#ZONE=unbound.se ; aws route53 list-hosted-zones | jq --arg zone "${ZONE}." '.HostedZones[] | select(.Name == $zone) | .Id'
