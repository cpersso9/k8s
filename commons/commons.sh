#!/usr/bin/env bash

RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color

local:prereqs() {
  type kubectl >/dev/null 2>&1 || { echo >&2 "I require kubectl but it's not installed.  Aborting."; exit 1; }
  if [[ $(kubectl config get-contexts | grep docker-for-desktop) ]]; then
    IS_MINIKUBE=
  else
    echo >&2 "I require 'docker-for-desktop' context to run locally but it's not available.  Aborting.";
    exit 1
  fi
  type helm >/dev/null 2>&1 || { echo >&2 "I require helm but it's not installed.  Aborting."; exit 1; }
  AWS_KEYS="${AWS_ACCESS_KEY_ID?Need AWS_ACCESS_KEY_ID as ENVIRONMENT variable} ${AWS_SECRET_ACCESS_KEY?Need AWS_SECRET_ACCESS_KEY as ENVIRONMENT variable}}"
}

commons:pre_reqs() {
  ! getopt --test > /dev/null
  if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo "I’m sorry, `getopt --test` failed in this environment."
    exit 1
  fi
  local tools_needed="kops aws kubectl sed jq"
  local missing_tools=
  for tool in $tools_needed; do
    if ! type $tool >/dev/null 2>&1 ; then
      missing_tools="${tool}, ${missing_tools}"
    fi
  done
  if [[ -n "${missing_tools}" ]]; then
    echo "Needed tool(s): ${missing_tools}"
    exit 1
  fi
}
